import pytest
from python_at_2021.tests.svmelny.homework_1.tasks.task_1 import check_password


def test_password_8_items_valid():
    assert check_password('QWrTY13*') is True


def test_password_more_8_items_valid():
    assert check_password('qWerty123*') is True


def test_password_less_8_items_non_valid():
    assert check_password('QrY3*') is False


def test_password_30_items_valid():
    assert check_password('qwertyQE5*qwertyQE5*qwertyQE5*') is True


def test_password_more_30_items_non_valid():
    assert check_password('qwertyQE5*qwertyQE5*qwertyQE5*qwertyQE5*qwertyQE5*qwertyQE5*b') is False


def test_password_less_30_items_valid():
    assert check_password('Qwerty123123/*') is True


@pytest.mark.parametrize('password, expected_result', [
    ('aABC123*/+', True),
    ('bABC123*/+', True),
    ('cABC123*/+', True),
    ('dABC123*/+', True),
    ('eABC123*/+', True),
    ('fABC123*/+', True),
    ('gABC123*/+', True),
    ('hABC123*/+', True),
    ('iABC123*/+', True),
    ('jABC123*/+', True),
    ('kABC123*/+', True),
    ('lABC123*/+', True),
    ('mABC123*/+', True),
    ('nABC123*/+', True),
    ('oABC123*/+', True),
    ('pABC123*/+', True),
    ('qABC123*/+', True),
    ('rABC123*/+', True),
    ('sABC123*/+', True),
    ('tABC123*/+', True),
    ('uABC123*/+', True),
    ('vABC123*/+', True),
    ('wABC123*/+', True),
    ('xABC123*/+', True),
    ('yABC123*/+', True),
    ('zABC123*/+', True)
])
def test_password_lower_case_letter(password, expected_result):
    assert check_password(password) is expected_result


@pytest.mark.parametrize('password, expected_result', [
    ('Aabc123*/+', True),
    ('Babc123*/+', True),
    ('Cabc123*/+', True),
    ('Dabc123*/+', True),
    ('Eabc123*/+', True),
    ('Fabc123*/+', True),
    ('Gabc123*/+', True),
    ('Habc123*/+', True),
    ('Iabc123*/+', True),
    ('Jabc123*/+', True),
    ('Kabc123*/+', True),
    ('Labc123*/+', True),
    ('Mabc123*/+', True),
    ('Nabc123*/+', True),
    ('Oabc123*/+', True),
    ('Pabc123*/+', True),
    ('Qabc123*/+', True),
    ('Rabc123*/+', True),
    ('Sabc123*/+', True),
    ('Tabc123*/+', True),
    ('Uabc123*/+', True),
    ('Vabc123*/+', True),
    ('Wabc123*/+', True),
    ('Xabc123*/+', True),
    ('Yabc123*/+', True),
    ('Zabc123*/+', True)
])
def test_password_upper_case_letter(password, expected_result):
    assert check_password(password) is expected_result


@pytest.mark.parametrize('password, expected_result', [
    ('Qwerty_0*/+', True),
    ('Qwerty_1*/+', True),
    ('Qwerty_2*/+', True),
    ('Qwerty_3*/+', True),
    ('Qwerty_4*/+', True),
    ('Qwerty_5*/+', True),
    ('Qwerty_6*/+', True),
    ('Qwerty_7*/+', True),
    ('Qwerty_8*/+', True),
    ('Qwerty_9*/+', True)
])
def test_password_number(password, expected_result):
    assert check_password(password) is expected_result


@pytest.mark.parametrize('password, expected_result', [
    ('Qwerty123!', True),
    ('Qwerty123"', True),
    ('Qwerty123#', True),
    ('Qwerty123$', True),
    ('Qwerty123%', True),
    ('Qwerty123&', True),
    ("Qwerty123'", True),
    ('Qwerty123(', True),
    ('Qwerty123)', True),
    ('Qwerty123*', True),
    ('Qwerty123+', True),
    ('Qwerty123,', True),
    ('Qwerty123-', True),
    ('Qwerty123.', True),
    ('Qwerty123/', True),
    ('Qwerty123:', True),
    ('Qwerty123;', True),
    ('Qwerty123<', True),
    ('Qwerty123=', True),
    ('Qwerty123>', True),
    ('Qwerty123?', True),
    ('Qwerty123@', True),
    ('Qwerty123[', True),
    ('Qwerty123\\', True),
    ('Qwerty123]', True),
    ('Qwerty123^', True),
    ('Qwerty123_', True),
    ('Qwerty123`', True),
    ('Qwerty123{', True),
    ('Qwerty123|', True),
    ('Qwerty123}', True),
    ('Qwerty123~', True)
])
def test_password_special_character(password, expected_result):
    assert check_password(password) is expected_result

