import pytest
from python_at_2021.tests.svmelny.homework_1.tasks.task_2 import CONFIG, if_debug_on


def test_if_debug_on_original_value():
    assert if_debug_on()


def test_if_debug_on_return_false(monkeypatch):
    monkeypatch.setitem(CONFIG, 'debug', False)
    assert if_debug_on() is False


def test_if_debug_on_no_key(monkeypatch):
    monkeypatch.delitem(CONFIG, 'debug', True)
    with pytest.raises(KeyError):
        if_debug_on()
