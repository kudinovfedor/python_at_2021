import pytest, os, textwrap, copy
from appium import webdriver

from appium.webdriver.common.appiumby import AppiumBy as By
from appium.options.common.base import AppiumOptions


EXECUTOR = 'http://localhost:4723'
ANDROID_BASE_CAPS = {
    'app': os.path.abspath('./tests/android_test/ApiDemos-debug.apk'),
    'platformName': 'Android',
    'automationName': 'UiAutomator2',
    "noSign": "true",
    'platformVersion': os.getenv('ANDROID_PLATFORM_VERSION') or '11.0',
    'deviceName': os.getenv('ANDROID_DEVICE_VERSION') or 'Android',
}


@pytest.fixture
def driver(request):
    calling_request = request._pyfuncitem.name

    caps = copy.copy(ANDROID_BASE_CAPS)
    caps['name'] = calling_request
    caps['appActivity'] = request.node.get_closest_marker('activity').args[0]  # default activity

    options = AppiumOptions()
    options.load_capabilities(caps)
    driver = webdriver.Remote(command_executor=EXECUTOR,
                              options=options)

    driver.implicitly_wait(10)
    yield driver
    driver.quit()


PACKAGE = 'io.appium.android.apis'
SEARCH_ACTIVITY = '.app.SearchInvoke'
ALERT_DIALOG_ACTIVITY = '.app.AlertDialogSamples'


@pytest.mark.activity(SEARCH_ACTIVITY)
def test_should_send_keys_to_search_box_and_then_check_the_value(driver):

    search_box_element = driver.find_element(By.ID, 'txt_query_prefill')
    search_box_element.send_keys('Hello world!')

    on_search_requested_button = driver.find_element(By.ID, 'btn_start_search')
    on_search_requested_button.click()

    search_text = driver.find_element(By.ID, 'android:id/search_src_text')
    search_text_value = search_text.text

    assert 'Hello world!' == search_text_value


@pytest.mark.activity(ALERT_DIALOG_ACTIVITY)
def test_should_click_a_button_that_opens_an_alert_and_then_dismisses_it(driver):

    open_dialog_button = driver.find_element(By.ID, 'io.appium.android.apis:id/two_buttons')
    open_dialog_button.click()

    alert_element = driver.find_element(By.ID, 'android:id/alertTitle')
    alert_text = alert_element.text

    assert textwrap.dedent('''\
    Lorem ipsum dolor sit aie consectetur adipiscing
    Plloaso mako nuto siwuf cakso dodtos anr koop.''') == alert_text

    close_dialog_button = driver.find_element(By.ID, 'android:id/button1')
    close_dialog_button.click()
