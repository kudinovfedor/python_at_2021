import pytest
from selenium import webdriver


@pytest.fixture()
def driver(request):
    driver = webdriver.Chrome()
    driver.maximize_window()
    url_marker = request.node.get_closest_marker('url')
    driver.get(*url_marker.args)
    yield driver
    driver.quit()
