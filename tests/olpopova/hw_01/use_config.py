"""
Create 2 tests
in 1: if_debug_on should return original value from CONFIG
in 2: if_debug_on should return False
After test ends, CONFIG should stay the same as it was no matter if test passes or fails.

Also test if 'debug' key is not present - KeyError will happen
"""
from unittest.mock import Mock

import pytest

CONFIG = {
    "debug": True,
    "b": "abc",
    "c": 123
}


def if_debug_on():
    return CONFIG["debug"]


def test_if_debug_default():
    assert if_debug_on() is True


def test_if_debug_false(monkeypatch):
    monkeypatch.setitem(CONFIG, 'debug', False)
    assert if_debug_on() is False


def test_if_debug_error_monkeypatch(monkeypatch):
    monkeypatch.delitem(CONFIG, 'debug')
    with pytest.raises(KeyError):
        if_debug_on()


def test_if_debug_error():
    mock = Mock(side_effect=KeyError('foo'))
    with pytest.raises(KeyError):
        mock()
