import string

import pytest


def check_password(word: str) -> bool:
    """
    Length 8 - 30 symbols
    At least 1 letter in upper case
    At least 1 letter in lower case
    At least 1 special character
    At least 1 number
    """

    has_letters = set(word).intersection(string.ascii_lowercase)
    has_big_letters = set(word).intersection(string.ascii_uppercase)
    has_numbers = set(word).intersection(string.digits)
    has_symbols = set(word).intersection(string.punctuation)
    has_spaces = ' ' in word
    is_right_length = 8 <= len(word) <= 30

    return all([has_letters,
                has_big_letters,
                has_numbers,
                has_symbols,
                not has_spaces,
                is_right_length])


@pytest.mark.parametrize(['password', 'expected'], [
    ("sdfjJLsj123!@#", True),
    ('sdfjJLsj123!@#sdfjJLsj123!@#sd', True),
    ('j123!@#', False),
    ('sdfjJLsj123!@#sdfjJLsj123!@#sdf', False),
    ('j123 !@#', False),
    ('j1 2 3 !@#', False),
    ('12345678', False),
    ('ssssssss', False),
    ('SSSSSSSS', False),
    ('ssssSSSS', False),
    ('ssSSSS#$', False),
    ('!@#$%^&*()_+?><|\":}{\\[]', False),
])
def test_check_password(password, expected):
    assert check_password(password) is expected


@pytest.mark.parametrize("letter", string.ascii_uppercase)
def test_all_upper_letters(letter):
    """
    test you can use all letters
    """

    val = "aaa{}123!".format(letter)
    assert check_password(val) is True


@pytest.mark.parametrize("letter", string.ascii_lowercase)
def test_all_letters(letter):
    """
    test you can use all letters
    """

    val = "AAA{}123!".format(letter)
    assert check_password(val) is True


@pytest.mark.parametrize("num", string.digits)
def test_all_numbers(num):
    """
    test you can use all numbers
    """

    val = "aaa{}AAA!".format(num)
    assert check_password(val) is True


@pytest.mark.parametrize("symbol", string.punctuation)
def test_all_symbols(symbol):
    """
    test you can use all symbols
    """

    val = "aaa{}AAA1".format(symbol)
    assert check_password(val) is True
