import pytest
from selenium.webdriver import ActionChains as AC
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


JS_CLICK_SCRIPT = "arguments[0].click();"
ALERTS = By.XPATH, "//span[@class='group-header'][contains(., 'Alerts, Frame & Windows')]"
WIDGET_HEADER = "//span[@class='group-header'][contains(., 'Widgets')]"
WIDGETS = By.XPATH, f"{WIDGET_HEADER}//div[@class='header-right']"
FRAMES = By.XPATH, f"{ALERTS[1]}/..//span[text()= 'Frames']"
SLIDER_MENU = By.XPATH, f"{WIDGET_HEADER}/..//span[text()= 'Slider']"
SLIDER = By.XPATH, "//input[@type='range']"


@pytest.mark.url('https://demoqa.com/alertsWindows')
def test_switching_between_frames(driver):
    """
    Verify switching between frames
    """
    # Step-1:  Navigate to Frames
    frames_element = driver.find_element(*FRAMES)
    driver.execute_script(JS_CLICK_SCRIPT, frames_element)

    # Step-2: Switch between frames and back to main frame
    driver.switch_to.frame('frame1')
    driver.switch_to.default_content()
    driver.find_element(*ALERTS).click()


@pytest.mark.url('https://demoqa.com/automation-practice-form')
def test_left_menu_options(driver):
    """
    Verify left meny options
    """

    # Step-1: Click on Widgets menu option
    waiter = WebDriverWait(driver, 10)
    widget_menu_element = driver.find_element(*WIDGETS)
    waiter.until(expected_conditions.element_to_be_clickable(widget_menu_element))
    driver.execute_script("window.scrollTo(0, document.body.scrollHeight)")
    widget_menu_element.click()

    # Step-2: Click on Slider menu option
    driver.execute_script("window.scrollTo(0, document.body.scrollHeight)")
    slider_menu_element = driver.find_element(*SLIDER_MENU)
    waiter.until(expected_conditions.element_to_be_clickable(slider_menu_element))
    slider_menu_element.click()


@pytest.mark.url('https://demoqa.com/slider')
def test_slider_movement(driver):
    """
    Verify that user is able to move slider button
    """

    # Step-1: Move slider
    actions = AC(driver)
    initial_value = int(driver.find_element(By.ID, "sliderValue").get_attribute('value'))
    slider_element = driver.find_element(*SLIDER)
    actions.move_to_element(slider_element).click_and_hold(slider_element).move_by_offset(100, 0).release().perform()
    actual_value = int(driver.find_element(By.ID, "sliderValue").get_attribute('value'))
    assert initial_value != actual_value
