import time

import pytest
from selenium.common import NoSuchElementException
from selenium.webdriver.common.by import By


HOST = "https://www.saucedemo.com"
LOGIN = "standard_user"
PASSORD = "secret_sauce"


def test_big_ugly(session):

    session.get(HOST)

    # login
    session.find_element(By.ID, "user-name").send_keys(LOGIN)
    session.find_element(By.ID, "password").send_keys(PASSORD)
    session.find_element(By.ID, "login-button").click()

    # get items
    elements = session.find_elements(By.CSS_SELECTOR, ".inventory_list .inventory_item")
    assert len(elements) == 6

    # add 1 item
    price_0 = elements[0].find_element(By.XPATH, ".//*[@class='pricebar']//*[@class='inventory_item_price']").text
    price_0 = float(price_0.lstrip("$"))
    elements[0].find_element(By.XPATH, ".//*[@class='pricebar']//button").click()

    # add 3rd item
    price_2 = elements[2].find_element(By.XPATH, ".//*[@class='pricebar']//*[@class='inventory_item_price']").text
    price_2 = float(price_2.lstrip("$"))
    elements[2].find_element(By.XPATH, ".//*[@class='pricebar']//button").click()

    # go to cart
    cart = session.find_element(By.ID, "shopping_cart_container")
    cart_badge = cart.find_element(By.XPATH, ".//*[contains(@class, 'shopping_cart_badge')]")
    assert cart_badge.text == '2'

    cart.click()

    # chck cart page, checkout
    items = session.find_elements(By.CSS_SELECTOR, ".cart_list .cart_item")
    assert len(items) == 2

    session.find_element(By.ID, "checkout").click()

    # fill checkout
    session.find_element(By.ID, "first-name").send_keys("Jonh")
    session.find_element(By.ID, "last-name").send_keys("Adams")
    session.find_element(By.ID, "postal-code").send_keys("001011")
    session.find_element(By.ID, "continue").click()

    assert str(price_0 + price_2) in session.find_element(By.XPATH, "//*[@class='summary_subtotal_label']").text

    session.find_element(By.ID, "finish").click()
    session.find_element(By.ID, "back-to-products").click()

    cart = session.find_element(By.ID, "shopping_cart_container")
    with pytest.raises(NoSuchElementException):
        cart.find_element(By.XPATH, ".//*[contains(@class, 'shopping_cart_badge')]")
