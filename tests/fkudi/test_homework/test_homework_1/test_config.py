import pytest

from homework.homework_1.use_config import if_debug_on, CONFIG


def test_default():
    assert if_debug_on() is True


def test_default_false(monkeypatch):
    monkeypatch.setitem(CONFIG, "debug", False)
    assert if_debug_on() is False


def test_default_error(monkeypatch):
    monkeypatch.delitem(CONFIG, "debug")
    with pytest.raises(KeyError):
        if_debug_on()
