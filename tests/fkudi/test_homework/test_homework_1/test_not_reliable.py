import pytest
import random

from homework.homework_1.not_reliable_function import not_reliable


def test_not_reliable(mocker):
    mocker.patch.object(random, "random", return_value=0.5)
    assert not_reliable() == 0.5


def test_not_reliable_error(mocker):
    mocker.patch.object(random, "random", return_value=0.49)
    with pytest.raises(RuntimeError, match="Res is less than 0.5"):
        not_reliable()
