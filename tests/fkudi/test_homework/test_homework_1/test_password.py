import pytest
from string import ascii_lowercase, digits, punctuation, whitespace

from homework.homework_1.check_password import check_password


@pytest.mark.parametrize(["char"], ascii_lowercase)
def test_upper_char(char):
    word = "{}a11111!".format(char.upper())
    assert check_password(word)


@pytest.mark.parametrize(["char"], ascii_lowercase)
def test_char(char):
    word = "A{}11111!".format(char)
    assert check_password(word)


@pytest.mark.parametrize(["char"], digits)
def test_digit(char):
    word = "Aaaaaa{}!".format(char)
    assert check_password(word)


@pytest.mark.parametrize(["char"], punctuation)
def test_spec(char):
    word = "Aaaaaa1{}".format(char)
    assert check_password(word)


@pytest.mark.parametrize(["password", "res"], [
    ["Aaaaa1!", False],
    ["Aaaaaa1!", True],
    ["Aaaaaa1!" + "a" * 21, True],
    ["Aaaaaa1!" + "a" * 22, True],
    ["Aaaaaa1!" + "a" * 23, False]
])
def test_length(password, res):
    assert check_password(password) == res


@pytest.mark.parametrize("whitespace_char", whitespace)
@pytest.mark.parametrize(["password", "res"], [
    [" " * 30 + "Aaaaaa1!", True],   # ignore spaces?
    ["Aaaaaa1!" + " " * 30, True],   # ignore spaces?
    ["Aaa  aaa1!", False],  # space in the middle?
])
def test_spaces_checks(password, res, whitespace_char):
    password = password.replace(" ", whitespace_char)
    assert check_password(password) == res
