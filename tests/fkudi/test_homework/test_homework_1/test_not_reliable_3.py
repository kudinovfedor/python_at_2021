import pytest

from homework.homework_1 import not_reliable_function_3
from homework.homework_1.not_reliable_function_3 import not_reliable


@pytest.mark.parametrize(["items", "res"], [
    [[0.888], (0.888, 1)],
    [[0.887, 0.888], (0.888, 2)],
    [[0.887] * 9 + [0.888], (0.888, 10)],
    [[0.887] * 10 + [0.888], (0.888, 11)]
])
def test_not_reliable(mocker, items, res):
    mocker.patch.object(not_reliable_function_3, "random", side_effect=items)
    assert not_reliable() == res


def test_not_reliable_error(mocker):
    mocker.patch.object(not_reliable_function_3, "random", side_effect=[0.887] * 11)
    with pytest.raises(RuntimeError, match="Its too much, total tries: 11"):
        not_reliable()
