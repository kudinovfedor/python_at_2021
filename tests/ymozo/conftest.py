import pytest
from selenium.webdriver import Chrome


@pytest.fixture
def driver():
    driver = Chrome()
    driver.implicitly_wait(0.5)
    driver.maximize_window()
    yield driver
    driver.quit()
