import pytest
from tests.ymozo.hw_1.check_pass_corrected import check_password


@pytest.mark.parametrize(["password", "res"], [
    ("Ab123!#", False),     # 7 characters
    ("Abc123!#", True),     # 8 characters
    ("wg&(!qrR(QSv%3lwrlGIH2QRzBIab", True),    # 29 characters
    ("wg&(!qrR(QSv%3lwrlGIH2QRzBIab$", True),   # 30 characters
    ("wg&(!qrR(QSv%3lwrlGIH2QRzBIab$1", False),  # 31 characters
    ("abcd123!#", False),
    ("ABCD123!#", False),
    ("Abcdefg!#", False),
    ("Abcdef!#", False),
    ("Ab123456", False),
    ("Abc123 !#", False)
])
def test_correct_pass(password, res):
    test_pass = check_password(password)
    assert test_pass == res
