import string


def check_password(word):
    """
    Length 8 - 30 symbols
    At least 1 letter in upper case
    At least 1 letter in lower case
    At least 1 special character
    """

    has_letters = set(word).intersection(string.ascii_lowercase)
    has_big_letters = set(word).intersection(string.ascii_uppercase)
    has_numbers = set(word).intersection(string.digits)
    has_symbols = set(word).intersection(string.punctuation)
    has_spaces = " " in word

    correct_pass_len = 8 <= len(word) <= 30

    return all([has_letters,
                has_big_letters,
                has_numbers,
                has_symbols,
                not has_spaces,
                correct_pass_len])


assert check_password("sdfjJLsj123!@#") is True

print("All checks passed")