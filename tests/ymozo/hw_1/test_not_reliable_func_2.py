import pytest

from homework.homework_1 import not_reliable_function_2
from homework.homework_1.not_reliable_function_2 import not_reliable


def test_not_reliable_func(mocker):
    mocker.patch.object(not_reliable_function_2, 'random', return_value=0.6)
    assert not_reliable() == 0.6


def test_not_reliable_error(mocker):
    mocker.patch.object(not_reliable_function_2, "random", return_value=0.4)
    with pytest.raises(RuntimeError):
        not_reliable()
