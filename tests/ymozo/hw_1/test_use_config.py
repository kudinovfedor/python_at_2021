import pytest
from homework.homework_1.use_config import if_debug_on, CONFIG


def test_if_debug_on():
    assert if_debug_on() is True


def test_if_debug_of(monkeypatch):
    monkeypatch.setitem(CONFIG, 'debug', False)
    assert if_debug_on() is False


def test_key_error(monkeypatch):
    monkeypatch.delitem(CONFIG, name='debug')
    with pytest.raises(KeyError):
        if_debug_on()
