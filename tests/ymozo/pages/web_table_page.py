from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from tests.ymozo.pages.registration_form import RegFormPage


class RowItemComponent:
    """
    represents RowItem
    """

    EDIT_BUTTON = (By.XPATH, ".//span[@class = 'mr-2']")
    DELETE_BUTTON = (By.XPATH, ".//span[contains(@id, 'delete-record')]")
    CELLS_LOCATOR = (By.XPATH, ".//div[@class = 'rt-td']")

    def __init__(self, web_element: WebElement):
        self.element = web_element

    def click_btn(self, locator):
        btn = self.element.find_element(*locator)
        btn.click()

    def click_edit(self):
        self.click_btn(self.EDIT_BUTTON)

    def click_delete(self):
        self.click_btn(self.DELETE_BUTTON)

    def get_cells(self):
        return self.element.find_elements(*self.CELLS_LOCATOR)


class WebTablePage:
    """
    Represents Table
    """

    ROWS_LOCATOR = (By.XPATH, "//div[@class='rt-tr-group']")
    ADD_BUTTON = (By.ID, 'addNewRecordButton')
    SEARCH_FIELD = (By.XPATH, "//input[@class='form-control']")

    def __init__(self, driver):
        self.driver = driver

    def click_btn(self, locator):
        btn = self.driver.find_element(*locator)
        btn.click()

    def click_add_btn(self):
        self.click_btn(self.ADD_BUTTON)

    def get_rows(self):
        rows = self.driver.find_elements(*self.ROWS_LOCATOR)
        return [RowItemComponent(row) for row in rows if len(row.text.strip()) != 0]

    def fill_field(self, locator, value):
        field = self.driver.find_element(*locator)
        field.clear()
        field.send_keys(value)

    def fill_search_field(self, value):
        self.fill_field(self.SEARCH_FIELD, value)

    def create_element(self):
        self.click_add_btn()
        return RegFormPage(self.driver)
