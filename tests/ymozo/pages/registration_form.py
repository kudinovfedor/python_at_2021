from selenium.webdriver.common.by import By


class RegFormPage:
    FIRST_NAME_FIELD = (By.ID, 'firstName')
    LAST_NAME_FIELD = (By.ID, 'lastName')
    EMAIL_FIELD = (By.ID, 'userEmail')
    AGE_FIELD = (By.ID, 'age')
    SALARY_FIELD = (By.ID, 'salary')
    DEPARTMENT_FIELD = (By.ID, 'department')
    SUBMIT_BUTTON = (By.ID, 'submit')

    def __init__(self, driver):
        self.driver = driver

    def click_btn(self, locator):
        btn = self.driver.find_element(*locator)
        btn.click()

    def fill_field(self, locator, value):
        field = self.driver.find_element(*locator)
        field.clear()
        field.send_keys(value)

    def fill_first_name_field(self, value):
        self.fill_field(self.FIRST_NAME_FIELD, value)

    def fill_last_name_field(self, value):
        self.fill_field(self.LAST_NAME_FIELD, value)

    def fill_email_field(self, value):
        self.fill_field(self.EMAIL_FIELD, value)

    def fill_age_field(self, value):
        self.fill_field(self.AGE_FIELD, value)

    def fill_salary_field(self, value):
        self.fill_field(self.SALARY_FIELD, value)

    def fill_department_field(self, value):
        self.fill_field(self.DEPARTMENT_FIELD, value)

    def click_submit_btn(self):
        return self.click_btn(self.SUBMIT_BUTTON)

