from tests.ymozo.pages.registration_form import RegFormPage
from tests.ymozo.pages.web_table_page import WebTablePage


def test_register_new_user(driver):
    driver.get("https://demoqa.com/webtables")

    web_page = WebTablePage(driver)
    reg_form = web_page.create_element()

    reg_form.fill_first_name_field("Chandler")
    reg_form.fill_last_name_field("Bing")
    reg_form.fill_email_field("chandler_bing@mail.com")
    reg_form.fill_age_field(34)
    reg_form.fill_salary_field(3000)
    reg_form.fill_department_field("Counter")
    reg_form.click_submit_btn()

    rows = web_page.get_rows()
    rows_count = len(rows)
    last_row = rows[-1]
    cells = last_row.get_cells()

    assert 'Chandler' in cells[0].text
    assert rows_count == 4


def test_edit_user(driver):
    driver.get("https://demoqa.com/webtables")

    rows = WebTablePage(driver).get_rows()
    rows[0].click_edit()

    reg_form = RegFormPage(driver)
    reg_form.fill_first_name_field("Joey")
    reg_form.fill_last_name_field("Tribiani")
    reg_form.click_submit_btn()

    edited_row = rows[0]
    cells = edited_row.get_cells()
    assert 'Joey' in cells[0].text


def test_delete_user(driver):
    driver.get("https://demoqa.com/webtables")

    web_page = WebTablePage(driver)
    rows = web_page.get_rows()
    row_amount_before = len(web_page.get_rows())
    rows[0].click_delete()
    row_amount_after = len(web_page.get_rows())

    assert row_amount_before != row_amount_after


def test_search_by_user_name(driver):
    driver.get("https://demoqa.com/webtables")
    web_table = WebTablePage(driver)

    row_amount_before = len(web_table.get_rows())
    web_table.fill_search_field("Kierra")
    row_amount_after = len(web_table.get_rows())

    assert row_amount_before != row_amount_after
