from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains


def test_double_click(driver):
    action = ActionChains(driver)
    driver.get("https://demoqa.com/buttons")
    button = driver.find_element(By.ID, "doubleClickBtn")
    action.double_click(button).perform()
    get_confirmation_message = driver.find_element(By.ID, "doubleClickMessage")
    confirmation_message = "You have done a double click"
    assert get_confirmation_message.text == confirmation_message


def test_right_click(driver):
    action = ActionChains(driver)
    driver.get("https://demoqa.com/buttons")
    button = driver.find_element(By.ID, "rightClickBtn")
    action.context_click(button).perform()
    get_confirmation_message = driver.find_element(By.ID, "rightClickMessage")
    confirmation_message = "You have done a right click"
    assert get_confirmation_message.text == confirmation_message


def test_dynamic_click(driver):
    driver.get("https://demoqa.com/buttons")
    button = driver.find_element(By.XPATH, "//button[text()='Click Me']")
    button.click()
    get_confirmation_message = driver.find_element(By.ID, "dynamicClickMessage")
    confirmation_message = "You have done a dynamic click"
    assert get_confirmation_message.text == confirmation_message

