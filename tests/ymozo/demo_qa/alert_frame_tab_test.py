from selenium.webdriver.common.by import By
HOST = "https://demoqa.com/"


def test_alert(driver):
    driver.get(HOST + "alerts")
    alert_btn = driver.find_element(By.CSS_SELECTOR, '#confirmButton')
    alert_btn.click()

    alert = driver.switch_to.alert
    alert.accept()

    confirm_message = driver.find_element(By.CSS_SELECTOR, '#confirmResult')
    assert confirm_message.text == 'You selected Ok'


def test_new_tab(driver):
    driver.get(HOST + "browser-windows")
    parent_window_locator = driver.find_element(By.XPATH, "//div[@class = 'main-header']")
    new_window_btn = driver.find_element(By.ID, "tabButton")
    parent_window = driver.current_window_handle
    new_window_btn.click()

    all_windows = driver.window_handles
    driver.switch_to.window(all_windows[-1])
    new_window_message = driver.find_element(By.ID, "sampleHeading")

    assert new_window_message.text == "This is a sample page"
    driver.switch_to.window(parent_window)
    assert parent_window_locator.text == "Browser Windows"
