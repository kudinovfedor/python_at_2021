from requests import session

from tests.ymozo.demoqa_api.config import HOST


class ApiClient:
    host = HOST

    def __init__(self, login, password):
        self.login = login
        self.password = password
        self.user_id = None
        self.client = session()

    @property
    def token(self):
        header = self.client.headers.get("Authorization")
        return header.replace("Bearer ", "") if header else None

    @token.setter
    def token(self, token):
        if token:
            self.client.headers["Authorization"] = "Bearer {}".format(token)
        else:
            del self.token

    @token.deleter
    def token(self):
        self.client.headers.pop("Authorization", None)

    def create_new_user(self):
        res = self.client.post(self.host + "/Account/v1/User",
                               json={"userName": self.login,
                                     "password": self.password})
        res.raise_for_status()
        return res.json()

    def generate_token(self):
        res = self.client.post(self.host + "/Account/v1/GenerateToken",
                               json={"userName": self.login,
                                     "password": self.password})
        res.raise_for_status()
        return res.json()

    def user_login(self):
        res = self.client.post(self.host + "/Account/v1/login",
                               json={"userName": self.login,
                                     "password": self.password})
        res.raise_for_status()
        return res.json()

    def authorized(self) -> bool:
        res = self.client.post(self.host + "/Account/v1/Authorized",
                               json={"userName": self.login,
                                     "password": self.password})
        res.raise_for_status()
        return res.text == 'true'

    def del_user(self):

        res = self.client.delete(f"{self.host}/Account/v1/User/{self.user_id}")

        res.raise_for_status()

    def get_books(self):
        res = self.client.get(self.host + "/BookStore/v1/Books")
        res.raise_for_status()
        return res.json()

    def get_user(self):
        res = self.client.get(f"{self.host}/Account/v1/User/{self.user_id}")

        res.raise_for_status()
        return res.json()

    def add_books_to_user(self, isbns):
        res = self.client.post(self.host + "/BookStore/v1/Books",
                               json={"userId": self.user_id,
                                     "collectionOfIsbns": [{"isbn": isbn} for isbn in isbns]})
        res.raise_for_status()
        return res.json()

    def remove_books(self):
        params = {"UserId": self.user_id}
        res = self.client.delete(self.host + "/BookStore/v1/Books", params=params)
        res.raise_for_status()

    def user_exist(self):
        res = self.client.post(self.host + "/Account/v1/Authorized",
                               json={"userName": self.login,
                                     "password": self.password})
        return res.status_code == 200

    def ger_or_create_user(self):

        if self.user_exist():
            user = self.user_login()
            self.user_id = user["userId"]
            self.token = user["token"] or self.generate_token()["token"]
            assert self.authorized()
        else:
            self.user_id = self.create_new_user()["userID"]
            self.token = self.generate_token()["token"]
            assert self.authorized()
