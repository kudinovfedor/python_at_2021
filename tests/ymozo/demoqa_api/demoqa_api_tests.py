import pytest
from tests.ymozo.demoqa_api.demoqa_api_client import ApiClient
from tests.ymozo.demoqa_api.config import USER_NAME, PASSWORD


@pytest.fixture()
def api_client():
    client = ApiClient(USER_NAME, PASSWORD)
    client.ger_or_create_user()
    client.remove_books()
    yield client

    client.del_user()


@pytest.fixture()
def isbn_list(api_client):

    # making a list of books
    books = api_client.get_books()["books"]
    isbn_list = []

    for i in range(3):
        isbn = books[i]["isbn"]
        isbn_list.append(isbn)

    return isbn_list


def test_add_books(api_client, isbn_list):

    api_client.add_books_to_user(isbn_list)
    expected_num_of_books = api_client.get_user()["books"]

    assert len(expected_num_of_books) == 3


def test_remove_books(api_client, isbn_list):

    api_client.add_books_to_user(isbn_list)
    expected_num_of_books = api_client.get_user()["books"]

    assert len(expected_num_of_books) == 3

    api_client.remove_books()
    expected_num_of_books = api_client.get_user()["books"]

    assert len(expected_num_of_books) == 0
