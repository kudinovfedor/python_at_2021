import pytest
from selenium.common import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains

BASE_URL = "https://demoqa.com/radio-button"


def test_select_yes(driver):
    driver.get(BASE_URL)

    yes_radio = driver.find_element(By.ID, "yesRadio")

    actions = ActionChains(driver)
    actions.move_to_element(yes_radio).click().perform()

    message = driver.find_element(By.CSS_SELECTOR, ".mt-3").text
    assert message == "You have selected Yes"


def test_select_impressive(driver):
    driver.get(BASE_URL)

    impressive_radio = driver.find_element(By.ID, "impressiveRadio")

    actions = ActionChains(driver)
    actions.move_to_element(impressive_radio).click().perform()

    message = driver.find_element(By.CSS_SELECTOR, ".mt-3").text
    assert message == "You have selected Impressive"


def test_no_is_disabled(driver):
    driver.get(BASE_URL)

    no_radio = driver.find_element(By.ID, "noRadio")

    actions = ActionChains(driver)
    actions.move_to_element(no_radio).click().perform()

    with pytest.raises(NoSuchElementException):
        driver.find_element(By.CSS_SELECTOR, ".mt-3")


def test_no_message_initially(driver):
    driver.get(BASE_URL)

    messages = driver.find_elements(By.CSS_SELECTOR, ".mt-3")
    assert len(messages) == 0
