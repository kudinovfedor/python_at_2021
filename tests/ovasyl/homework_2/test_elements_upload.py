import os
from selenium.webdriver.common.by import By

BASE_URL = "https://demoqa.com/upload-download"
PART_OF_UPLOAD_URL = "C:\\fakepath\\"
UPLOAD_FILE_NAME = "flag.png"


def test_select_yes(driver):
    driver.get(BASE_URL)

    current_directory = os.path.dirname(os.path.abspath(__file__))
    file_path = os.path.join(current_directory, UPLOAD_FILE_NAME)

    upload = driver.find_element(By.ID, "uploadFile")
    upload.send_keys(file_path)

    path = driver.find_element(By.ID, "uploadedFilePath").text
    assert path == PART_OF_UPLOAD_URL + UPLOAD_FILE_NAME