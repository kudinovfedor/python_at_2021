import pytest

CONFIG = {
    "debug": True,
    "b": "abc",
    "c": 123
}


def test_if_debug_on_returns_original_value():
    assert if_debug_on() == CONFIG["debug"]


def test_if_debug_on_returns_false(monkeypatch):
    monkeypatch.setitem(CONFIG, "debug", False)
    assert if_debug_on() == False


def test_if_debug_on_raises_key_error(monkeypatch):
    monkeypatch.delitem(CONFIG, "debug")
    with pytest.raises(KeyError):
        if_debug_on()


def if_debug_on():
    return CONFIG["debug"]
