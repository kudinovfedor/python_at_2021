import pytest

LOWER_LETTERS = "abcdefghijklmnopqrstuvwxyz"
CAPITAL_LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
NUMBERS = "0123456789"
SYMBOLS = "!@#$%^&*()_+?><|\":}{\\[]-=`~';:,./"


def check_password(word):
    """
    Length 8 - 30 symbols
    At least 1 letter in upper case
    At least 1 letter in lower case
    At least 1 number
    At least 1 special character
    """

    has_letters = set(word).intersection(LOWER_LETTERS)
    has_big_letters = set(word).intersection(CAPITAL_LETTERS)
    has_numbers = set(word).intersection(NUMBERS)
    has_symbols = set(word).intersection(SYMBOLS)

    not_short = len(word) >= 8
    not_long = len(word) <= 30

    return all([has_letters,
                has_big_letters,
                has_numbers,
                has_symbols,
                not_short,
                not_long])


@pytest.mark.parametrize("password, expected_result", [
    ("Sh0rt!!", False),
    ("T00larggggggggggggggggggggggg3!" * 31, False),
    ("N0rmal!!", True),
    ("L0ngggggggggggggggggggggggggg!", True),
    ("UPPERCASE1@#", False),
    ("lowercase1@#", False),
    ("NoNumber!", False),
    ("N0SpecialCharacters", False),
])
def test_check_password(password, expected_result):
    assert check_password(password) == expected_result


@pytest.mark.parametrize("letter", LOWER_LETTERS)
def test_all_lower_letters(letter):
    val = "AAA{}123!".format(letter)
    assert check_password(val) is True


@pytest.mark.parametrize("letter", CAPITAL_LETTERS)
def test_all_capital_letters(letter):
    val = "aaa{}123!".format(letter)
    assert check_password(val) is True


@pytest.mark.parametrize("number", NUMBERS)
def test_all_numbers(number):
    val = "AAA{}aaa!".format(number)
    assert check_password(val) is True


@pytest.mark.parametrize("symbols", SYMBOLS)
def test_all_symbols(symbols):
    val = "AAA1{}aaa".format(symbols)
    assert check_password(val) is True
