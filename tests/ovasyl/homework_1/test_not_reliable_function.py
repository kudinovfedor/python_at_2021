import pytest
import random


def not_reliable():
    res = random.random()
    if res < 0.5:
        raise RuntimeError("Res is less than 0.5:  {}".format(res))
    return res


def test_not_reliable_raises_runtime_error(monkeypatch):
    monkeypatch.setattr(random, 'random', lambda: 0.4)

    with pytest.raises(RuntimeError):
        not_reliable()


def test_not_reliable_returns_value(monkeypatch):
    monkeypatch.setattr(random, 'random', lambda: 0.5)

    value = not_reliable()
    assert isinstance(value, float)
    assert 0.5 == value
