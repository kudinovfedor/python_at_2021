"""
cover with tests
fix functionality according to docstring
"""


import pytest


def check_password(word):
    """
    Length 8 - 30 symbols
    At least 1 letter in upper case
    At least 1 letter in lower case
    At least 1 special character
    """

    has_letters = set(word).intersection("abcdefghijklmnopqrstuvwxyz")
    has_big_letters = set(word).intersection("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
    has_numbers = set(word).intersection("012356789")
    has_symbols = set(word).intersection("-!@#$%^&*()_+?><|\":}{\\[]")

    not_short = len(word) >= 8
    not_long = len(word) < 30

    return all([has_letters,
                has_big_letters,
                has_numbers,
                has_symbols,
                not_short,
                not_long])


assert check_password("sdfjJLsj123!@#") is True

print("All checks passed")


@pytest.mark.parametrize("letter", "abcdefghijklmnopqrstuvwxyz")
def test_all_letters(letter):
    """
    test you can use all letters
    """

    val = "AAA{}123!".format(letter)
    assert check_password(val) is True


@pytest.mark.parametrize("digit", "1234567890")
def test_all_digits(digit):
    """
    test you can use all digits
    """

    val = "{}ABCdef!".format(digit)
    assert check_password(val) is True


@pytest.mark.parametrize("symbol", "!@#$%^&*()+-?><|:}{\[]\"\\~;'")
def test_all_symbols(symbol):
    """
    test you can use all symbols
    """

    val = "{}ABCdef1".format(symbol)
    assert check_password(val) is True
