from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ES
from selenium.webdriver.support.ui import WebDriverWait as Wait

from tests.ikarchevska.config import HOST2


def test_simple_dynamic_buttons(driver):
    driver.get(HOST2 + "/styled/dynamic-buttons-simple.html")
    start_button = driver.find_element(By.XPATH, "//*[@id='button00']")
    start_button.click()
    one_button = driver.find_element(By.XPATH, "//*[@id='button01']")
    one_button.click()
    Wait(driver, 10).until(ES.presence_of_element_located((By.XPATH, "//*[@id='button02']")))
    two_button = driver.find_element(By.XPATH, "//*[@id='button02']")
    two_button.click()
    Wait(driver, 10).until(ES.presence_of_element_located((By.XPATH, "//*[@id='button03']")))
    three_button = driver.find_element(By.XPATH, "//*[@id='button03']")
    three_button.click()

    result = driver.find_element(By.XPATH, "//*[@id='buttonmessage']")
    assert result.text == "All Buttons Clicked"
