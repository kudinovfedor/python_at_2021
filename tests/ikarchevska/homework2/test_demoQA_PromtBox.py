from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ES
from selenium.webdriver.support.ui import WebDriverWait as Wait

from tests.ikarchevska.config import HOST


def test_alert_prompt_box(driver):
    driver.get(HOST + "/alerts")
    el = driver.find_element(By.XPATH, "//*[@id='promtButton']")
    el.click()
    Wait(driver, 1).until(ES.alert_is_present())
    obj = driver.switch_to.alert
    obj.send_keys('test value')
    obj.accept()

    result = driver.find_element(By.XPATH, "//*[@id='promptResult']")
    assert result.text == "You entered test value"


