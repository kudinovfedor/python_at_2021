from selenium.webdriver.common.by import By

from tests.ikarchevska.config import HOST


def test_first_name(driver):
    driver.get(HOST + "/text-box")
    el = driver.find_element(By.CSS_SELECTOR, "#userName")
    el.clear()
    el.send_keys("Test")
    el = driver.find_element(By.CSS_SELECTOR, "#submit")
    # scrolldown
    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    el.click()
    el = driver.find_element(By.CSS_SELECTOR, "p#name")
    assert el.text == "Name:Test"


def test_email(driver):
    driver.get(HOST + "/text-box")
    el = driver.find_element(By.XPATH, "//*[@id='userEmail']")
    el.clear()
    el.send_keys("Test@mail.com")
    el = driver.find_element(By.XPATH, "//*[@id='submit']")
    # scrolldown
    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    el.click()
    el = driver.find_element(By.XPATH, "//*[@id='email']")
    assert el.text == "Email:Test@mail.com"


def test_book_store(driver):
    driver.get(HOST + "/text-box")
    el = driver.find_element(By.XPATH, "//div[@class='header-text' and text()='Elements']")
    el.click()

    el = driver.find_element(By.XPATH, "//div[@class='header-text' and text()='Book Store Application']")

    # remove banners because intercept element click
    driver.execute_script("document.querySelector('#fixedban').innerHTML = ''")
    driver.execute_script("document.querySelector('footer').outerHTML = ''")

    el.click()

    driver.implicitly_wait(0.1)
    el = driver.find_element(By.XPATH, "//div[@class='header-text' and text()='Book Store Application']/"
                                       "ancestor::div[@class='element-group']//"
                                       "*[span[@class='text'][text()='Book Store']]")
    el.click()
    el = driver.find_element(By.XPATH, "//div[@class='main-header' and text()='Book Store']")
    assert el.text == "Book Store"
