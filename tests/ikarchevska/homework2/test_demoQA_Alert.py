from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ES
from selenium.webdriver.support.ui import WebDriverWait as Wait

from tests.ikarchevska.config import HOST


def test_alert(driver):
    driver.get(HOST + "/alerts")
    el = driver.find_element(By.XPATH, "//*[@id='timerAlertButton']")
    el.click()
    alert = Wait(driver, 6).until(ES.alert_is_present())

    assert alert.text == "This alert appeared after 5 seconds"
