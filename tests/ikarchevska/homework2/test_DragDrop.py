from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains as AC

from tests.ikarchevska.config import HOST2


def test_drag_and_drop(driver):
    driver.get(HOST2 + "/styled/drag-drop-javascript.html")
    source_element1 = driver.find_element(By.XPATH, "//*[@id='draggable1']")
    source_element2 = driver.find_element(By.XPATH, "//*[@id='draggable2']")
    target_element = driver.find_element(By.XPATH, "//*[@id='droppable1']")
    actions = AC(driver)
    actions.drag_and_drop(source_element1, target_element).perform()
    actions.drag_and_drop(source_element2, target_element).perform()

    assert target_element.text == "Get Off Me!"
