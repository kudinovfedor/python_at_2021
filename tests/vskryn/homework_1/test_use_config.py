"""
Create 2 tests
in 1: if_debug_on should return original value from CONFIG
in 2: if_debug_on should return False
After test ends, CONFIG should stay the same as it was no matter if test passes or fails.

Also test if 'debug' key is not present - KeyError will happen
"""
import pytest

CONFIG = {"debug": True, "b": "abc", "c": 123}


def if_debug_on():
    return CONFIG["debug"]


def test_debug_true():
    """Test that "debug" is True"""
    assert if_debug_on() is True


def test_debug_false(monkeypatch):
    """Test that "debug" is False"""

    monkeypatch.setitem(CONFIG, "debug", False)

    assert if_debug_on() is False


def test_key_error(monkeypatch):
    """Test that KeyError raises if "debug" is deleted"""

    monkeypatch.delitem(CONFIG, "debug", raising=False)

    with pytest.raises(KeyError):
        if_debug_on()
