"""
cover with tests
fix functionality according to docstring
"""
import pytest


def check_password(word):
    """
    Length 8 - 30 symbols
    At least 1 letter in upper case
    At least 1 letter in lower case
    At least 1 special character
    """

    has_letters = set(word).intersection("abcdefghiklmnopqrstuvwxyz")
    has_big_letters = set(word).intersection("ABCDEFGHIJKMNOPQRSTUVWXYZ")
    has_numbers = set(word).intersection("012356789")
    has_symbols = set(word).intersection("!@#$%^&*()_+?><|\":}{\\[]")

    not_short = len(word) >= 8
    not_long = len(word) < 30

    return all([has_letters, has_big_letters, has_numbers, has_symbols, not_short, not_long])


assert check_password("sdfjJLsj123!@#") is True

print("All checks passed")


@pytest.mark.parametrize(
    'password, expected',
    [
        pytest.param('Aa0!aaa', False, marks=pytest.mark.negative, id="7 symbols (negative)"),
        pytest.param('Aa0!aaaa', True, marks=pytest.mark.positive, id="8 symbols (positive)"),
        pytest.param('Aa0!aaaab', True, marks=pytest.mark.positive, id="9 symbols (positive)"),
        pytest.param('At5#%NJIKMpl,mnjiuy7bvgt%rfcxd', True, marks=pytest.mark.positive, id="30 symbols (positive)"),
        pytest.param('At5#%NJIKMpl,mnjiuy7bvgt%rfcxd+', False, marks=pytest.mark.negative, id="31 symbols (negative)"),
        pytest.param('br0@zafhv', False, marks=pytest.mark.negative, id="No upper case letter (negative)"),
        pytest.param('&QWERT3JNM', False, marks=pytest.mark.negative, id="No lower case letter (negative)"),
        pytest.param('Pn@!fcw$a', False, marks=pytest.mark.negative, id="No numbers (negative)"),
        pytest.param('1Efijnbvkhj', False, marks=pytest.mark.negative, id="No special characters (negative)"),
    ],
)
def test_check_password_basic_scenarios(password, expected):
    """Test password requirements"""

    assert check_password(password) is expected


@pytest.mark.parametrize("letter", "abcdefghijklmnopqrstuvwxyz")
def test_all_lowercase_letters(letter):
    """Test that all lowercase letters are available"""
    password = 'A10!{}1111'.format(letter)

    assert check_password(password) is True


@pytest.mark.parametrize("letter", "ABCDEFGHIJKLMNOPQRSTUVWXYZ")
def test_all_uppercase_letters(letter):
    """Test that all uppercase letters are available"""
    password = 'la0!{}aaab'.format(letter.upper())

    assert check_password(password) is True


@pytest.mark.parametrize("number", "0123456789")
def test_all_numbers(number):
    """Test that all numbers are available"""
    password = 'Aau!{}aaab'.format(number)

    assert check_password(password) is True


@pytest.mark.parametrize("spec_characters", "@,.$#&*][}{=-)(`~+;\'/\\^:><|\"!#%&_")
def test_spec_characters(spec_characters):
    """Test that all spec characters are available"""
    password = 'Aau1{}aaab'.format(spec_characters)

    assert check_password(password) is True
