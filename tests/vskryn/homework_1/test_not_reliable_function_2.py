
import pytest
from python_at_2021.homework.homework_1 import not_reliable_function_2
from python_at_2021.homework.homework_1.not_reliable_function_2 import not_reliable


@pytest.mark.parametrize("mock_value", [0.499999999999999, 0.5, 0.5000000000000001])
def test_not_reliable(mocker, mock_value):
    """Test if value is less than 0.5, the function raises RuntimeError. If value is more than 0.5, it returns value"""

    mocker.patch.object(not_reliable_function_2, "random", return_value=mock_value)

    if mock_value < 0.5:
        with pytest.raises(RuntimeError) as exc_info:
            not_reliable()

        assert str(exc_info.value) == f"Res is less than 0.5:  {mock_value}"

    else:
        assert not_reliable() == mock_value
