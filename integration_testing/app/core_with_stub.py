from abc import ABC, abstractmethod
from typing import List, NamedTuple, Optional

from app.session import Session
from app.models import Book, Author


class StoredBook(NamedTuple):
    book_id: int
    title: str


class Storage(ABC):
    @abstractmethod
    def search_author(self, first_name: str, last_name: str) -> Optional[int]:
        pass

    @abstractmethod
    def add_author(self, first_name: str, last_name: str) -> int:
        pass

    @abstractmethod
    def publish_book(self, author_id: int, title: str) -> int:
        pass

    @abstractmethod
    def get_book(self, book_id: int) -> StoredBook:
        pass

    @abstractmethod
    def search_by_title(self, title: str) -> List[StoredBook]:
        pass


class RdbStorage(Storage):
    def __init__(self, session: Session):
        self.session = session

    def search_author(self, first_name: str, last_name: str) -> Optional[int]:
        author = self.session.query(Author.author_id).filter(
            Author.first_name == first_name,
            Author.last_name == last_name,
        ).one_or_none()
        if author:
            return author.author_id

    def add_author(self, first_name: str, last_name: str) -> int:
        author = Author(first_name=first_name, last_name=last_name)
        self.session.add(author)
        self.session.commit()
        return author.author_id

    def publish_book(self, author_id: int, title: str) -> int:
        book = Book(author_id=author_id, title=title)
        self.session.add(book)
        self.session.commit()
        return book.book_id

    def get_book(self, book_id: int) -> StoredBook:
        book = self.session.query(Book).filter(Book.book_id == book_id).one_or_none()
        return StoredBook(book.book_id, book.title)

    def search_by_title(self, title: str) -> List[StoredBook]:
        books = self.session.query(Book).filter(Book.title.like(f"%{title}%")).all()
        return [StoredBook(book.book_id, book.title) for book in books]


class BookStore:
    def __init__(self, storage: Storage):
        self.storage = storage

    def publish_new_book(
        self, title: str, author_name: str, author_last_name: str
    ):
        try:
            author_id = self.storage.search_author(author_name, author_last_name)
        except NotFound:
            author_id = self.storage.add_author(author_name, author_last_name)

        self.storage.publish_book(author_id, title)
