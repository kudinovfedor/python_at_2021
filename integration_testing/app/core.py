from typing import List, Optional

from app.session import Session
from app.models import Book, Author


def publish_book(author_id: int, title: str, session: Session) -> int:
    book = Book(author_id=author_id, title=title)
    session.add(book)
    session.commit()
    return book.book_id


def get_book(book_id: int, session: Session) -> Book:
    return session.query(Book).filter(Book.book_id == book_id).one_or_none()


def search_by_title(title: str, session: Session) -> List[Book]:
    return session.query(Book).filter(Book.title.like(f"%{title}%")).all()


def search_author(first_name: str, last_name: str, session: Session) -> Optional[int]:
    author = session.query(Author.author_id).filter(
        Author.first_name == first_name,
        Author.last_name == last_name,
    ).one_or_none()
    if author:
        return author.author_id


def add_author(first_name: str, last_name: str, session: Session) -> int:
    author = Author(first_name=first_name, last_name=last_name)
    session.add(author)
    session.commit()
    return author.author_id


def publish_new_book(
    title: str, author_name: str, author_last_name: str, session: Session
):
    author_id = search_author(author_name, author_last_name, session)
    if author_id is None:
        author_id = add_author(author_name, author_last_name, session)
    publish_book(author_id, title, session)
