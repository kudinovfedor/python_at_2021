from typing import List, Optional
from unittest import mock

import pytest

from app.core_with_stub import BookStore, Storage
from app.models import Book


class StorageStub(Storage):
    def search_author(self, first_name: str, last_name: str) -> Optional[int]:
        pass

    def add_author(self, first_name: str, last_name: str) -> int:
        pass

    def publish_book(self, author_id: int, title: str) -> int:
        pass

    def get_book(self, book_id: int) -> Book:
        pass

    def search_by_title(self, title: str) -> List[Book]:
        pass


@pytest.fixture
def storage():
    return StorageStub()


@pytest.fixture
def book_store(storage):
    return BookStore(storage)


def test_publish_new_book(book_store, storage):
    storage.search_author = mock.MagicMock(raises=AssertionError)
    book_store.publish_new_book(
        "awesome",
        "name",
        "surname"
    )
