import pytest
from uuid import uuid4

from app.models import Book, Author
from app.session import Session
from app.core import publish_book, get_book


@pytest.fixture
def session():
    with Session() as session:
        yield session


@pytest.fixture
def author(session):
    author = Author(
        first_name=f"test_first_name_{uuid4()}",
        last_name=f"test_last_name_{uuid4()}",
    )
    session.add(author)
    session.commit()
    yield author
    session.delete(author)
    session.commit()


@pytest.fixture
def title():
    return f"test_book_{uuid4()}"


@pytest.fixture
def book_cleanup(session, title):
    yield
    session.query(Book).filter(Book.title == title).delete()
    session.commit()


@pytest.mark.usefixtures("book_cleanup")
def test_publish_book(author: Author, session, title):
    book_id = publish_book(author.author_id, title, session)
    book = get_book(book_id, session)
    assert book.title == title
