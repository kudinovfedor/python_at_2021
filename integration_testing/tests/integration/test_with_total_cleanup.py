import pytest
from uuid import uuid4

from app.models import Book, Author
from app.session import Session
from app.core import publish_book, get_book


@pytest.fixture
def session():
    with Session() as session:
        session.query(Book).delete()
        session.query(Author).delete()
        session.commit()
        yield session
        session.query(Book).delete()
        session.query(Author).delete()
        session.commit()


@pytest.fixture
def author(session):
    author = Author(
        first_name=f"test_first_name_{uuid4()}",
        last_name=f"test_last_name_{uuid4()}",
    )
    session.add(author)
    session.commit()
    return author


def test_publish_book(author: Author, session):
    title = f"test_book_{uuid4()}"
    book_id = publish_book(author.author_id, title, session)
    book = get_book(book_id, session)
    assert book.title == title
