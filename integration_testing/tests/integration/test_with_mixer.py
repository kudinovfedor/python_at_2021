from uuid import uuid4

import pytest
from mixer.backend.sqlalchemy import Mixer

from app.models import Author, Book
from app.session import Session
from app.core import publish_book, get_book, search_by_title


@pytest.fixture
def session():
    with Session() as session:
        yield session


@pytest.fixture
def mixer(session):
    return Mixer(session=session)


@pytest.fixture
def author(mixer, session):
    author_ = mixer.blend(Author, last_name=mixer.FAKE, first_name=mixer.FAKE)
    yield author_
    session.delete(author_)
    session.commit()


@pytest.fixture
def book(mixer):
    return mixer.blend(Book, title=mixer.FAKE, author__first_name="Bob")
    # return mixer.blend(Book, title=mixer.FAKE)


@pytest.fixture
def title():
    return f"test_book_{uuid4()}"


def test_publish_book(author: Author, session, title):
    book_id = publish_book(author.author_id, title, session)
    book = get_book(book_id, session)
    assert book.title == title


def test_get_book_exists(book, session):
    book = get_book(book.book_id, session)
    assert book is not None


# ########################################################
# PARAMETRIZE
# ########################################################


@pytest.mark.parametrize("search_str", ["first", "second"])
def test_book_search(mixer, search_str, session):
    add_books_to_search(mixer, search_str)
    books = search_by_title(search_str, session)
    assert books
    assert len(books) >= 5


def add_books_to_search(mixer, search_str):
    mixer.cycle(5).blend(
        Book,
        title=mixer.sequence(lambda x: f"{uuid4()} {search_str} {uuid4()}")
    )


def test_book_search_clean(search_str, books_to_search, session):
    books = search_by_title(search_str, session)
    for book in books_to_search:
        assert book in books


@pytest.fixture(params=["first", "second"])
def search_str(request):
    return request.param


@pytest.fixture
def books_to_search(mixer, author, search_str, session):
    books = mixer.cycle(5).blend(
        Book,
        author=author,
        title=mixer.sequence(lambda x: f"{uuid4()} {search_str} {uuid4()}")
    )
    yield books
    for book in books:
        session.delete(book)
    session.commit()
